using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    public class HitboxController : MonoBehaviour, IDamageable
    {
        public event Action<int> OnDamage;

        public void Damage(int amount)
        {
            OnDamage?.Invoke(amount);
        }

        public void Push(Vector3 direction, float impulse)
        {
            GetComponent<Rigidbody>().AddForce(direction * impulse, ForceMode.Impulse);
        }
    }

    public interface IDamageable
    {
        public event Action<int> OnDamage;
        void Damage(int amount);
    }
}