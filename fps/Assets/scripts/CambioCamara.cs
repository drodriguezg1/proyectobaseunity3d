using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioCamara : MonoBehaviour
{
    [SerializeField] private GameObject primeraPersona;
    [SerializeField] private GameObject terceraPersona;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.O))
            firstPerson();
        if (Input.GetKey(KeyCode.P))
            thirdPerson();
    }

    public void firstPerson()
    {   
        primeraPersona.SetActive(true);
        terceraPersona.SetActive(false); 
    }
    public void thirdPerson()
    {
        primeraPersona.SetActive(false);
        terceraPersona.SetActive(true);
    }
}
