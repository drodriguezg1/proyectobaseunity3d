using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class PJcontroller : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 3f;

    [SerializeField]
    private float m_RotationSpeed = 360f;
    private float m_MouseSensitivity = 1f;

    Vector3 m_Movement = Vector3.zero;
    private Rigidbody m_Rigidbody;

    //Camera
    [SerializeField]
    private GameObject m_Camera;
    [SerializeField]
    private bool m_InvertY = false;

    [SerializeField]
    private LayerMask m_ShootMask;

    [SerializeField]
    private GameEvent pistola;
    [SerializeField]
    private GameEvent escopeta;

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;


    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Input = Instantiate(m_InputAsset);
        m_MovementAction = m_Input.FindActionMap("move").FindAction("pj");
        m_Input.FindActionMap("move").Enable();
         
    }

    void Update()
    {

        Vector2 move = m_MovementAction.ReadValue<Vector2>() * m_Speed;

        Vector3 oooo = new Vector3(move.x, 0f, move.y);
        oooo = m_Camera.transform.TransformDirection(oooo.normalized)*m_Speed;

        m_Rigidbody.velocity = new Vector3(oooo.x, m_Rigidbody.velocity.y, oooo.z);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Debug.Log("pistola");
            Shoot();
        }
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            Debug.Log("escopeta");
            Shoot2();
        }

    }

    private void Shoot()
    {
        pistola.Raise();

    }
    private void Shoot2()
    {
        escopeta.Raise();

    }
}



