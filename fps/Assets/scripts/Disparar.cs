using m17;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    public LayerMask m_ShootMask;
    public GameObject camera;
    public float dispersion;
    // Start is called before the first frame update
    public void disparo()
    {
        float x = Random.Range(-dispersion, dispersion);
        float y = Random.Range(-dispersion, dispersion);

        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, 20f, m_ShootMask))
        {
            Debug.Log($"He tocat {hit.collider.gameObject} ");
            Debug.DrawLine(this.transform.position, hit.point, Color.green, 2f);

            if (hit.collider.TryGetComponent<HitboxController>(out HitboxController target))
                target.Damage(10);

            //if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
            //    pushable.Push(m_Camera.transform.forward, 10);
        }
    }
    public void escopeta()
    {
        float x = Random.Range(-dispersion, dispersion);
        float y = Random.Range(-dispersion, dispersion);

        Vector3 direction = camera.transform.forward + x*camera.transform.right + y*camera.transform.up;
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, direction, out hit, 20f, m_ShootMask))
        {
            Debug.Log($"He tocat {hit.collider.gameObject}");
            Debug.DrawLine(this.transform.position, hit.point, Color.magenta, 2f);
            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                target.Damage(10);

            //if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
            //    pushable.Push(m_Camera.transform.forward, 10);
        }
    }
}
