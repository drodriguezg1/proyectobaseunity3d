using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    public class RagdollController : MonoBehaviour
    {
        private int m_HP = 100;

        private Rigidbody[] m_Bones;
        private Animator m_Animator;

        private void Awake()
        {
            m_Animator = GetComponent<Animator>();
            m_Bones = GetComponentsInChildren<Rigidbody>();
            Activate(false);

            foreach (Rigidbody bone in m_Bones)
                if(bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                    damageable.OnDamage += ReceiveDamage;
        }

        public void Activate(bool state)
        {
            foreach (Rigidbody bone in m_Bones)
                bone.isKinematic = !state;
            m_Animator.enabled = !state;
        }

        private void ReceiveDamage(int damage)
        {
            m_HP -= damage;
            Debug.Log("queda "+ m_HP +" vida");
            if (m_HP <= 0)
                Die();
        }

        private void Die()
        {
            foreach (Rigidbody bone in m_Bones)
                if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                    damageable.OnDamage -= ReceiveDamage;
            GetComponent<EnemigoController>().die();
            Activate(true);
        }
    }
}