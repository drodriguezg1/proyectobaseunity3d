
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.GraphicsBuffer;

public class EnemigoController : MonoBehaviour
{
    private enum SwitchMachineStates { DEAD, NONE, WALK, ATACK, PATROL, IDLE };
    [SerializeField]
    private SwitchMachineStates m_CurrentState;

    public NavMeshAgent agent;
    public Transform player;
    public LayerMask pj;

    //Variables Patrulla
    public Transform wp1;
    public Transform wp2;

    public string vaHacia;



    //Varialbes ataque
    public float cadencia = 2;
    bool atacando;

    //states
    public float areaDeteccion, areaAtaque;
    public bool Persiguiendo, Atacando;


    [SerializeField]
    private float tiempoEspera;
    private float espera;
    public LayerMask m_ShootMask;


    private void Awake()
    {
        player = GameObject.Find("pj").transform;
        agent = GetComponent<NavMeshAgent>();
        InitState(SwitchMachineStates.PATROL);

    }

    void Update()
    {
        UpdateState();
    }

    private void FixedUpdate()
    {
        Atacando = Physics.CheckSphere(transform.position, areaAtaque, pj);
        Persiguiendo = Physics.CheckSphere(transform.position, areaDeteccion, pj);

    }

    private void ChangeState(SwitchMachineStates newState)
    {
        //no caldria fer-ho, per evitem que un estat entri a si mateix
        //s possible que la nostra mquina ho permeti, per tant aix
        //no es faria sempre.
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    public void die()
    {
        ChangeState(SwitchMachineStates.DEAD);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {

            case SwitchMachineStates.WALK:


                break;

            case SwitchMachineStates.ATACK:
                InvokeRepeating("shoot", 0, cadencia);

                break;

            case SwitchMachineStates.PATROL:
                agent.SetDestination(wp1.position);


                break;

            case SwitchMachineStates.IDLE:

                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.WALK:
                break;

            case SwitchMachineStates.ATACK:
                CancelInvoke("shoot");

                break;
            case SwitchMachineStates.PATROL:

                break;
            case SwitchMachineStates.IDLE:

                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                agent.SetDestination(transform.position);

                if (Persiguiendo && !Atacando)
                {
                    ChangeState(SwitchMachineStates.WALK);
                }

                if (espera <= 0)
                {
                    ChangeState(SwitchMachineStates.PATROL);
                    espera = tiempoEspera;
                }
                else
                {
                    espera -= Time.deltaTime;
                }


                break;
            case SwitchMachineStates.WALK:

                if (Persiguiendo && Atacando)
                {
                    ChangeState(SwitchMachineStates.ATACK);
                }
                if (!Persiguiendo && !Atacando)
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }
                agent.SetDestination(player.position);


                break;
            case SwitchMachineStates.ATACK:

                agent.SetDestination(transform.position);
                transform.LookAt(player);

                if (!Atacando)
                {
                    ChangeState(SwitchMachineStates.WALK);
                }

                break;

            case SwitchMachineStates.PATROL:

                if (Persiguiendo && !Atacando)
                {
                    ChangeState(SwitchMachineStates.WALK);
                }
                else
                {
                    Vector3 distanciaWp1 = transform.position-wp1.position;
                    Vector3 distanciaWp2 = transform.position-wp2.position;
                    if (distanciaWp1.magnitude < 1.5f)
                    {
                        agent.SetDestination(wp2.position);
                        vaHacia = "wp2";

                    }
                    else if (distanciaWp2.magnitude < 1.5f)
                    {
                        agent.SetDestination(wp1.position);
                        vaHacia = "wp1";
                    }
                }

                break;
            default:
                break;
        }
    }

    public void shoot()
    {
        float dispersion = 0.4f;
        RaycastHit hit;

        float x = Random.Range(-dispersion, dispersion);

        Vector3 direction = transform.forward + new Vector3(x, 0, 0);

        if (Physics.Raycast(this.transform.position, direction, out hit, 20f, m_ShootMask))
        {
            Debug.Log($"He tocat {hit.collider.gameObject} ");
            Debug.DrawLine(this.transform.position, hit.point, Color.red, 2f);

            //if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
            //    target.Damage(10);

            //if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
            //    pushable.Push(m_Camera.transform.forward, 10);
        }
    }


    // Start is called before the first frame update



}
