using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    public float sens = 100f;
    float RotacionX = 0f;

    public Transform player;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

    }

    // Update is called once per frame
    void Update()
    {

        float verticall = Input.GetAxis("Mouse Y") * sens * Time.deltaTime;
        float horizontall = Input.GetAxis("Mouse X") * sens * Time.deltaTime;

        RotacionX -= verticall;
        RotacionX = Mathf.Clamp(RotacionX, -45f, 45f);
        //camara
        transform.localRotation = Quaternion.Euler(RotacionX, 0, 0);
        //player
        player.Rotate(Vector3.up, horizontall);


    }
}
